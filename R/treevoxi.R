#' @noRd
#' @importFrom stringr str_split
treevoxi <- function(tree.pc, max.edge.length, i.edge.length,
                     xc, yc, zc) {
  tree.pc.vox.i <- tree.pc |>
    dplyr::mutate(
      x.i.class = base::cut(x, breaks = seq(from = xc-2.5*max.edge.length,
                                            to = xc+2.5*max.edge.length,
                                            by = i.edge.length), include.lowest = T),
      y.i.class = base::cut(y, breaks = seq(from = yc-2.5*max.edge.length,
                                            to = yc+2.5*max.edge.length,
                                            by = i.edge.length), include.lowest = T),
      z.i.class = base::cut(z, breaks = seq(from = zc-2.5*max.edge.length,
                                            to = zc+2.5*max.edge.length,
                                            by = i.edge.length), include.lowest = T),
    ) |>
    dplyr::mutate(
      xyz.i.class = paste0(x.i.class, y.i.class, z.i.class)
    )
  
  xlimo <- stringr::str_split(as.character(tree.pc.vox.i$x.i.class[1]), ",")
  
  edge.leng <- substr(xlimo[[1]][2],1,nchar(xlimo[[1]][2])-1) |> as.numeric()-
    substr(xlimo[[1]][1],2,nchar(xlimo[[1]][1])) |> as.numeric()
  
  tree.pc.vox.i |>
    dplyr::group_by(xyz.i.class) |>
    dplyr::summarise(Npoints = dplyr::n()) |>
    nrow() -> Nvox
  
  return(data.frame(Nvox, edge.leng))
}
