# Tree Architectural Traits package

![license](https://img.shields.io/badge/Licence-GPL--3-blue.svg)


<img align="right" width="300" height="300" src= "images/hexagon_v3.png">

Install the following libraries (if not installed):

``` r
library(tidyverse)

library(rlas)
library(VoxR)

library(rgl)

library(geometry)
library(rgeos)
library(sp)


```


To install `TreeArchTraits` try with the following code:

``` r
remotes::install_git("https://gitlab.com/Puletti/tree_arch_traits")

library(TreeArchTraits)
```

### (1) Upload example tree data

``` r
mytree <- las2datatable("ExampleData/27_1_1_t31_wg.las")
```

### (2) Estimate traditional tree parameters (DBH and Total Tree Height)
``` r
treeparam <- TraditionalTreeParam(mytree)
```


### (3) Estimate box-dimension `Db`

``` r
myBd.list <- BoxDim(mytree, initial.box.res= round(treeparam$TTH)+.5)

myBd.list[[2]] %>%
  ggplot(aes(x.Db,y.Db)) +
  geom_point() +
  geom_abline(intercept=0, slope=1) +
  xlim(0,10) + 
  ylim(0,10) +
  stat_smooth(se = FALSE, method = lm)

myBd.list[[1]]

```

![Graphical illustration of derived linear regression for box-dimension calculation. The slope of the regression line equals the box-dimension of the tree.](images/Db_result.png)


### (4) Estimate stem parameters
``` r
tree.filename <- "ExampleData/27_1_1_t31_wg.las"
pc_tree <- las2datatable(tree.filename)
my.minZ <- pc_tree$Z

my.stem.filename <- "ExampleData/27_1_1_t31_stem.las"
my.Hvox <- .10 # metri di altezza del voxel

mystempar <- StemParam(my.stem.filename, my.minZ, my.Hvox)
mystempar
```

### (5) Estimate crown parameters
``` r
my.crown.filename <- "ExampleData/27_1_1_t31_crown.las"
mycrownpar <- CrownParam(my.crown.filename, Hvox = 0.10)
mycrownpar
```

### (6) Calculate total tree volume
``` r
TTV <- mystempar[[4]]$TSV + mycrownpar[[2]]$Bvol
```
