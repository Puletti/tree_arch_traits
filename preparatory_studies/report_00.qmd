---
title: "TreeArchTraits: checking the package"
format: pdf
editor: visual
date: 15-Apr-2023
---

```{r, echo=FALSE, message=F}
library(tidyverse)
library(TreeArchTraits)
```

This report aims to present some real data collected with TLS. All data were manually cleaned in the Trimble Real Works software.\
The main problem arised with the application of `BoxDim_adj` function using the tree *Castanea sativa*.

# Import file

Due to the file size and its analysis using BoxDim_adj, I prefer to save the result of `BoxDim_adj` as a Rdata file.

```{r, message=F}
CaSa <- "ascii/CaSa_frutt_t01.asc" |> 
  read_table(col_names = F) |> 
  rename(X = X1, Y = X2, Z = X3)
# CaSa_Db_adj.list <- BoxDim_adj(CaSa) |> as_tibble()
# save(CaSa_Db_adj.list, file = "Rdata/CaSa.Rdata")
load("Rdata/CaSa.Rdata")

CaSa_Db_adj.list |> print(n = 15)

```

Problems:\
1) `edge.leng` jumps from 15 to 2.50 (a big jump!);\
2) after row 9, `edge.leng` value is 0.300 six times, till row 14.\

## Unroll the function

#- set colnames of the first three columns as x, y, z\
names(pc_tree) \<- c('x', 'y', 'z')

#- find the max edge length\
`xyz.dims <- c(max(pc_tree$x, na.rm = T)-min(pc_tree$x, na.rm = T),`\
`max(pc_tree$y, na.rm = T)-min(pc_tree$y, na.rm = T),`\
`max(pc_tree$z, na.rm = T)-min(pc_tree$z, na.rm = T))`\

`max.edge.length \<- xyz.dims\[which(xyz.dims==max(xyz.dims))\]`\

#- define point cloud centroid coordinates\

`xc <- mean(pc_tree$x); yc <- mean(pc_tree$y); zc <- mean(pc_tree\$z)`\

`(myprogr <- (max.edge.length/seq(1, 150, 5)))`\
`pb <- txtProgressBar(min = 0, max = length(myprogr))`\

`res.i <- NULL`\
`for(i.edge.length in myprogr) {`\

```         
setTxtProgressBar(pb, which(myprogr == i.edge.length)) \

res.i <- res.i |> \
  dplyr::bind_rows( \
    treevoxi(pc_tree, max.edge.length, i.edge.length, xc, yc, zc) \
  ) \
```

}\

```{r}
pc_tree <- CaSa
names(pc_tree) <- c('x', 'y', 'z')

pc_tree[sample(1:nrow(pc_tree), size = 5000),] |> 
  ggplot(aes(x, z)) +
  geom_point()
```

```{r}
xyz.dims <- c(max(pc_tree$x, na.rm = T)-min(pc_tree$x, na.rm = T),
                max(pc_tree$y, na.rm = T)-min(pc_tree$y, na.rm = T),
                max(pc_tree$z, na.rm = T)-min(pc_tree$z, na.rm = T))
xyz.dims

(max.edge.length <- xyz.dims[which(xyz.dims==max(xyz.dims))])

xc <- mean(pc_tree$x); yc <- mean(pc_tree$y); zc <- mean(pc_tree$z)
xc; yc; zc

(myprogr <- (max.edge.length/seq(1, 150, 5)))
```

Something can be changed here

```{r}
myprogr.new <- max.edge.length/(2^(0:12))

(myprogr.new <- myprogr.new[myprogr.new >= 0.1])
```

Load specific function

```{r}
treevoxi <- function(tree.pc, max.edge.length, i.edge.length,
                     xc, yc, zc) {
  tree.pc.vox.i <- tree.pc |>
    dplyr::mutate(
      x.i.class = base::cut(x, breaks = seq(from = xc-2.5*max.edge.length,
                                            to = xc+2.5*max.edge.length,
                                            by = i.edge.length), include.lowest = T),
      y.i.class = base::cut(y, breaks = seq(from = yc-2.5*max.edge.length,
                                            to = yc+2.5*max.edge.length,
                                            by = i.edge.length), include.lowest = T),
      z.i.class = base::cut(z, breaks = seq(from = zc-2.5*max.edge.length,
                                            to = zc+2.5*max.edge.length,
                                            by = i.edge.length), include.lowest = T),
    ) |>
    dplyr::mutate(
      xyz.i.class = paste0(x.i.class, y.i.class, z.i.class)
    )
  
  xlimo <- stringr::str_split(as.character(tree.pc.vox.i$x.i.class[1]), ",")
  
  edge.leng <- substr(xlimo[[1]][2],1,nchar(xlimo[[1]][2])-1) |> as.numeric()-
    substr(xlimo[[1]][1],2,nchar(xlimo[[1]][1])) |> as.numeric()
  
  tree.pc.vox.i |>
    dplyr::group_by(xyz.i.class) |>
    dplyr::summarise(Npoints = dplyr::n()) |>
    nrow() -> Nvox
  
  return(data.frame(Nvox, edge.leng))
}

```

Testing the funcion with the new `myprogr` values

```{r}
pb <- txtProgressBar(min = 0, max = length(myprogr.new))

res.i <- NULL
  for(i.edge.length in myprogr.new) {
    
    setTxtProgressBar(pb, which(myprogr.new == i.edge.length))
    
    res.i <- res.i |>
      dplyr::bind_rows(
        treevoxi(pc_tree, max.edge.length, i.edge.length, xc, yc, zc)
      )
  }

res.i
```

Plotting result

```{r}
res.i |> 
  ggplot(aes(x = log(1/(edge.leng/max.edge.length)), y = log(Nvox) )) +
  geom_point() +
  geom_smooth(method = "lm", 
              color="black", 
              formula = y ~ x, 
              se = F,
              lty = 2,
              lwd = .5) +
  ylim(0,15)

```
