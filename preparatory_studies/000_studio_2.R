rm(list = ls())

library(tidyverse)
library(TreeLS)
library(VoxR)

list.files("inst/extdata")

stem <- TreeLS::readTLS("inst/extdata/27_1_1_t31_stem.las")
plot(stem)

pc_tree <- TreeArchTraits::las2datatable("inst/extdata/27_1_1_t31_stem.las")
range(pc_tree$Z)
(tree_h <- max(pc_tree$Z) - min(pc_tree$Z))

pc_tree %>% nrow # total number of points in the point cloud

initial.box.res <- 10 # altezza indicativa massima del primo voxel

x.Db <- y.Db <- NULL

# testing before the "for" statement
i <- 5 # voxel size: c(10, 5, 2.5, 1, 0.5, 0.1)

# voxelizzazione usando la altezza "i" come valore di box-edge
vox.i <- VoxR::vox(pc_tree, res = i)

# check if the number of points in the point cloud is maintained after voxalization
sum(vox.i$npts) == (pc_tree %>% nrow)

# check x and y range...
range(pc_tree$X); range(pc_tree$Y)

# ...against the voxels created in "vox.i"
vox.i

# Graphical check
rgl::points3d(pc_tree$X, pc_tree$Y, pc_tree$Z)
rgl::points3d(vox.i$x, vox.i$y, vox.i$z, size = 20, color = "blue")
rgl::grid3d("x", col = "red"); rgl::grid3d("y", col = "green")


# Box dimension
# Numerator: log(num_vox_with_points)
pvt <- 3 # pvt: point voxel threshold
(num_vox_with_points <- sum(vox.i$npts > pvt)) # n vox con più di "pvt" punti

x.Db <- c(x.Db, log(1/(i/initial.box.res)))
y.Db <- c(y.Db, log(num_vox_with_points))

## Ciclo for ----
x.Db <- y.Db <- NULL
for(i in c(10, 5, 2.5, 1, 0.5, 0.1)) {
  # voxelizzazione usando la altezza "i" come valore di box-edge
  vox.i <- VoxR::vox(pc_tree, res = i)
  
  # Numerator: log(num_vox_with_points)
  pvt <- 3 # pvt: point voxel threshold
  (num_vox_with_points <- sum(vox.i$npts > pvt)) # n vox con più di "pvt" punti
  
  x.Db <- c(x.Db, log(1/(i/initial.box.res)))
  y.Db <- c(y.Db, log(num_vox_with_points))
}

out.tibb <- tibble(x.Db, y.Db)

out.tibb <- out.tibb[-1,]

out.tibb %>% 
  ggplot(aes(x.Db, y.Db)) +
  geom_point() +
  geom_smooth(method = lm, formula = y ~ x, se = FALSE) +
  xlim(0, max(out.tibb$x.Db)+1) +
  ylim(0, max(out.tibb$y.Db)+1) +
  geom_abline()


reg <- lm(y.Db ~ x.Db, data = out.tibb)
reg$coefficients[[1]] # intercept: 
reg$coefficients[[2]] # slope: Db

# comments:
# despite the trunk is a quite cylindric volume,
# the Db is far from the theoretical vaue of "1"
