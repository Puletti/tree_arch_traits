#' @name BoxDim
#' 
#' @title Calculation of Db index (Seidel et al. 2019)
#' 
#' @description Estimates the box-dimensions (`Db`) as an integrative measure of tree architecture. 
#' `Db` is sensitive to both outer shape and internal structure of a tree. It is also scale independent and hence useful 
#' when trees of different sizes are to be compared. 
#' 
#' @usage BoxDim(pc_tree, pvt)
#' 
#' @param pc_tree A `data.table` obtained by las2datatable
#' @param pvt The number of points within a voxel to define it as "vegetation".
#' 
#' @return A `list` containing the estimated box-dimension as a measure of overall tree complexity, 
#' the box-dimension_intercept as a measure of tree size, 
#' and the whole `dataframe` as last element.
#' 
#' @seealso [BoxDim_adj()]
#' 
#' @importFrom ggplot2 ggplot
#' @importFrom stats lm coef
#' 
#' @details `Db` is determined by evaluating how many virtual boxes one needs to enclose all points of the 3D tree
#' and how the number of boxes changes with the ratio of the box size to the original box size.  
#' `Db` is then the slope of the fitted straight line through the scatterplot 
#' of the logarithm of the number of boxes
#' needed over the inverse of the logarithm of the used box-resolution.
#' For more details on `Db` calculations, please refer to Seidel et al. 2019 (*How a measure of structural complexity relates to architectural benefit-to-cost ratio, light availability and growth of trees*. 
#' Ecology and Evolution. DOI: 10.1002/ece3.5281
#' 
#' 
#' @examples
#' mytree <- las2datatable(system.file("extdata", "27_1_1_t31_wg.las", package="TreeArchTraits"))
#' treeparam <- TraditionalTreeParam(mytree)
#' 
#' myDb.list <- BoxDim(mytree, pvt = 3)
#'
#'myDb.list$df |> 
#'    ggplot2::ggplot(ggplot2::aes(x = x.Db, y = y.Db)) +
#'    ggplot2::geom_point() + 
#'    ggplot2::geom_smooth(method = "lm", color="black", formula = y ~ x) +
#'    ggplot2::xlim(0,6) + ggplot2::ylim(0,11.5) +
#'    ggplot2::geom_text(
#'      ggplot2::aes(2, 10, label = paste0("Db = ", round(myDb.list$Db, 2))
#'    ))
#'  
#' @export

BoxDim <- function(pc_tree, pvt, graph = T) {
  
  print("The first column is X coords, the second Y, the third Z")
  
  colnames(pc_tree)[1:3] <- c("X", "Y", "Z")
  
  initial.box.res <- c(max(pc_tree$X)-min(pc_tree$X),
                       max(pc_tree$Y)-min(pc_tree$Y),
                       max(pc_tree$Z)-min(pc_tree$Z)) |> max()
  
  box.resH <- initial.box.res/c(1,3,5,7)
  box.resL <- c(4, 2, 1, 0.50, 0.25, 0.10)
  
  box.res.all <- c(box.resH[box.resH>=5], box.resL)
  
  # Creating vertex coordinates of the initial "big" box (step-1)
  x.box.min <- mean(pc_tree$X)-(initial.box.res/2)
  x.box.max <- mean(pc_tree$X)+(initial.box.res/2)
  
  y.box.min <- mean(pc_tree$Y)-(initial.box.res/2)
  y.box.max <- mean(pc_tree$Y)+(initial.box.res/2)
  
  z.box.min <- min(pc_tree$Z)
  
  # Creating vertex coordinates of the initial "big" box (step-2)
  xyz.vertex0.initialBox <- tibble::tibble(
    x.vert = c(x.box.min, x.box.max, x.box.max, x.box.min),
    y.vert = c(y.box.min, y.box.min, y.box.max, y.box.max),
    z.vert = rep(z.box.min, 4)
  )
  
  xyz.vertexTOP.initialBox <- tibble::tibble(
    x.vert = c(x.box.min, x.box.max, x.box.max, x.box.min),
    y.vert = c(y.box.min, y.box.min, y.box.max, y.box.max),
    z.vert = rep(z.box.min+initial.box.res, 4)
  )
  
  x.Db <- y.Db <- NULL
  for(vox.size.i in box.res.all) {
    tab1 <- pc_tree |>
      dplyr::select(X,Y,Z) |>
      dplyr::filter(X>=x.box.min, X<=x.box.max,
                    
                    Y>=y.box.min, Y<=y.box.max,
                    
                    Z>=xyz.vertex0.initialBox$z.vert[1],
                    Z<=xyz.vertexTOP.initialBox$z.vert[1]
      ) |>
      dplyr::arrange(Z,X,Y) |>
      dplyr::mutate(
        Zclass = base::cut(Z, breaks = round(seq(from = xyz.vertex0.initialBox$z.vert[1],
                                                 to = xyz.vertexTOP.initialBox$z.vert[1],
                                                 by = vox.size.i),3), dig.lab= 5),
        Xclass = base::cut(X, breaks = seq(from = x.box.min,
                                           to = x.box.max,
                                           by=vox.size.i), include.lowest = T),
        Yclass = base::cut(Y, breaks = seq(from = y.box.min,
                                           to = y.box.max,
                                           by=vox.size.i), include.lowest = T))
    
    TreeVoxelGrid.df <- tab1 |>
      dplyr::mutate(
        Zclass=factor(Zclass, levels=unique(tab1$Zclass)),
        Xclass=factor(Xclass, levels=unique(tab1$Xclass)),
        Yclass=factor(Yclass, levels=unique(tab1$Yclass))) |>
      dplyr::count(Xclass, Yclass, Zclass, name = 'npoints') |>
      dplyr::mutate(Xclass = as.character(Xclass),
                    Xclass2 = substr(Xclass,2,(nchar(Xclass)-1))) |>
      dplyr::mutate(Yclass = as.character(Yclass),
                    Yclass2 = substr(Yclass,2,(nchar(Yclass)-1))) |>
      dplyr::mutate(Zclass = as.character(Zclass),
                    Zclass2 = substr(Zclass,2,(nchar(Zclass)-1)))
    
    
    # Box dimension
    # Numerator: log(num_vox_with_points)
    
    (num_vox_with_points <- sum(TreeVoxelGrid.df$npoints > pvt)) # n vox con più di "pvt" punti
    
    (x.Db <- c(x.Db, log(1/(vox.size.i/initial.box.res))))
    (y.Db <- c(y.Db, log(num_vox_with_points)))
  }
  
  out.tibb <- tibble::tibble(x.Db, y.Db) |>
    dplyr::filter(x.Db>0, y.Db>0)
  
  reg <- lm(y.Db ~ x.Db, data = out.tibb)
  
  return(list(
    Db = as.vector(reg$coefficients[2]), 
    Db_intercept = as.vector(reg$coefficients[1]),
    df = out.tibb)
    )
}
